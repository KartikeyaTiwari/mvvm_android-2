package in.raghav.EduApp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import in.raghav.RoomDB.Entity.Shopping;
import in.raghav.ViewModel.ShopViewModel;

public class MainActivity extends AppCompatActivity {
    private ShopViewModel shopViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        shopViewModel = new ViewModelProvider(this).get(ShopViewModel.class);
        shopViewModel.getAllitems().observe(this, new Observer<List<Shopping>>() {
            @Override
            public void onChanged(List<Shopping> shoppings) {
                Toast.makeText(MainActivity.this, "on Changed !!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}