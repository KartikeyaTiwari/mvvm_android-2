package in.raghav.RoomDB.Entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "shopping_table")
public class Shopping {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    private Long quantity;

    private String description;

    private float price;

    public Shopping(String name, Long quantity, String description, float price) {
        this.name = name;
        this.quantity = quantity;
        this.description = description;
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getQuantity() {
        return quantity;
    }

    public String getDescription() {
        return description;
    }

    public float getPrice() {
        return price;
    }
}
