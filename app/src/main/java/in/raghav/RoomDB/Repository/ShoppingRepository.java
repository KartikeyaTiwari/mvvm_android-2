package in.raghav.RoomDB.Repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import in.raghav.RoomDB.Dao.ShoppingDao;
import in.raghav.RoomDB.DataBase.ShoppingDatabase;
import in.raghav.RoomDB.Entity.Shopping;

public class ShoppingRepository {
    private ShoppingDao shopdao;
    private LiveData<List<Shopping>> allshoppingItem;

    public ShoppingRepository(Application application) {
        ShoppingDatabase shopdatabase = ShoppingDatabase.getInstance(application);
        shopdao = shopdatabase.shopdao();
        allshoppingItem = shopdao.getAllShoppingItemDetail();
    }

    public void insert(Shopping shop) {
        new insert_ShopItemAsyncTask(shopdao).execute(shop);
    }

    public void update(Shopping shop) {
        new update_ShopItemAsyncTask(shopdao).execute(shop);
    }

    public void delete(Shopping shop) {
        new delete_ShopItemAsyncTask(shopdao).execute(shop);

    }

    public void deleteAll() {
        new deleteAll_ShopItemAsyncTask(shopdao).execute();

    }

    public LiveData<List<Shopping>> getAllshoppingItem() {
        return allshoppingItem;
    }

    private static class insert_ShopItemAsyncTask extends AsyncTask<Shopping, Void, Void> {
        private ShoppingDao shopdao;

        private insert_ShopItemAsyncTask(ShoppingDao shopdao) {
            this.shopdao = shopdao;
        }

        @Override
        protected Void doInBackground(Shopping... shoppings) {
            shopdao.insert(shoppings[0]);
            return null;
        }
    }

    private static class update_ShopItemAsyncTask extends AsyncTask<Shopping, Void, Void> {
        private ShoppingDao shopdao;

        private update_ShopItemAsyncTask(ShoppingDao shopdao) {
            this.shopdao = shopdao;
        }

        @Override
        protected Void doInBackground(Shopping... shoppings) {
            shopdao.update(shoppings[0]);
            return null;
        }
    }

    private static class delete_ShopItemAsyncTask extends AsyncTask<Shopping, Void, Void> {
        private ShoppingDao shopdao;

        private delete_ShopItemAsyncTask(ShoppingDao shopdao) {
            this.shopdao = shopdao;
        }

        @Override
        protected Void doInBackground(Shopping... shoppings) {
            shopdao.delete(shoppings[0]);
            return null;
        }
    }

    private static class deleteAll_ShopItemAsyncTask extends AsyncTask<Void, Void, Void> {

        private ShoppingDao shopdao;

        private deleteAll_ShopItemAsyncTask(ShoppingDao shopdao) {
            this.shopdao = shopdao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            shopdao.deleteAll();
            return null;
        }
    }
}
