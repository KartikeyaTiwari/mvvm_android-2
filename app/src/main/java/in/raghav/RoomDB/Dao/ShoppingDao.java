package in.raghav.RoomDB.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import in.raghav.RoomDB.Entity.Shopping;

@Dao
public interface ShoppingDao {

    @Insert
    void insert(Shopping shop);

    @Update
    void update(Shopping shop);

    @Delete
    void delete(Shopping shop);

    @Query("DELETE FROM shopping_table")
    void deleteAll();

    @Query("Select * from shopping_table order by id")
    LiveData<List<Shopping>> getAllShoppingItemDetail();


}
