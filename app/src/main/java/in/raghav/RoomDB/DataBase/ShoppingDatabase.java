package in.raghav.RoomDB.DataBase;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import in.raghav.RoomDB.Dao.ShoppingDao;
import in.raghav.RoomDB.Entity.Shopping;

@Database(entities = {Shopping.class}, version = 1)
public abstract class ShoppingDatabase extends RoomDatabase {

    private static ShoppingDatabase instance;

    public abstract ShoppingDao shopdao();

    public static synchronized ShoppingDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    ShoppingDatabase.class, "shop_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDBAsyncTask(instance).execute();
        }
    };

    private static class PopulateDBAsyncTask extends AsyncTask<Void, Void, Void> {
        private ShoppingDao shoppingDao;

        private PopulateDBAsyncTask(ShoppingDatabase db) {
            shoppingDao = db.shopdao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            shoppingDao.insert(new Shopping("name1", (long) 123, "New data1", 45.3f));
            return null;
        }
    }
}
