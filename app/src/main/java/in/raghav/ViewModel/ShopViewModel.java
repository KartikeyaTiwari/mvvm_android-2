package in.raghav.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import in.raghav.RoomDB.Entity.Shopping;
import in.raghav.RoomDB.Repository.ShoppingRepository;

public class ShopViewModel extends AndroidViewModel {
    private ShoppingRepository shoppingRepository;
    private LiveData<List<Shopping>> allitems;

    public ShopViewModel(@NonNull Application application) {
        super(application);
        shoppingRepository = new ShoppingRepository(application);
        allitems = shoppingRepository.getAllshoppingItem();
    }

    public void insert(Shopping shop){
        shoppingRepository.insert(shop);
    }

    public void update(Shopping shop){
        shoppingRepository.update(shop);
    }

    public void delete(Shopping shop){
        shoppingRepository.delete(shop);
    }

    public void deleteAllItems(){
        shoppingRepository.deleteAll();
    }

    public LiveData<List<Shopping>> getAllitems() {
        return allitems;
    }
}
